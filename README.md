# Liberation

## About

An interactive client application, accessible via modern web browsers, usable by community members and service admins for purposes like looking up the Fedora Badges leaderboard, checking badges awarded to self or someone else etc. and adding new badges (and their related awarding conditions) etc. respectively by interacting with the Accolades API

## Read more

1. https://fedora-arc.readthedocs.io/en/latest/badges/prop_rewrite_entities.html#internal-entities
